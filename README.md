### ScreenSnap

Screenshot tool for X11 with a Motif GUI. This is a wrapper around 'maim' and uses XImaging as the default image viewer.


![screenshot](screenshots/dvscreensnap_sample.png)

#### Features

- Captures the screen with one click.
- Time delay up to 30 seconds.
- Hide or show mouse pointer.
- Select rectangular area or highlight single window to capture.
- Choice of PNG or JPEG output.
- Auto-timestamps each snap and places them in the home dir.
- Opens resulting image in a viewer.


#### Requirements

- Motif 2.3.8
- X11/Xlib
- libXinerama
- maim
- XImaging

#### Build Instructions

Compile:

```
make clean && make
```

Run:

```
./dvscreensnap
```

Install:

```
sudo make install
```

Uninstall:

```
sudo make uninstall
```

#### License

This software is distributed free of charge under the BSD Zero Clause license.