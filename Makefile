COMPILER = cc
SOURCE = screensnap.c
CFLAGS = -O2
#CFLAGS = -O2 -D_NO_XIMAGING -D_CDE_ROUND_TOGGLES
LDFLAGS = -lXm -lX11 -lXt -lXinerama
INSTDIR = /usr/local/bin
TARGET = ./dvscreensnap

.PHONY: all clean install uninstall

all: $(TARGET)

$(TARGET): $(SOURCE) 
	$(COMPILER) $(CFLAGS) $^ $(LDFLAGS) -o $@
clean:
	rm -f $(TARGET)

install: $(TARGET)
	install -m 755 $(TARGET) $(INSTDIR)

uninstall:
	rm -f $(INSTDIR)/$(notdir $(TARGET))
