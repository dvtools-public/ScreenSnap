/*
	ScreenSnap is a graphical wrapper built around the 'maim' 
	screenshot tool.
*/

/*
	BSD Zero Clause License (0BSD)

	Permission to use, copy, modify, and/or distribute this
	software for any purpose with or without fee is hereby
	granted.

	THE SOFTWARE IS PROVIDED AS IS AND THE AUTHOR DISCLAIMS
	ALL WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL
	IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO
	EVENT SHALL THE AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT,
	INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
	WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS,
	WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
	TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH
	THE USE OR PERFORMANCE OF THIS SOFTWARE.
*/


#include <stdlib.h> /* system() */
#include <stdio.h> /* snprintf(), fprintf() */
#include <string.h> /* strcpy() */
#include <time.h> /* time(), strftime() */

#include <X11/Shell.h> /* needed by toolkit core/vendorshell? */
#include <X11/Intrinsic.h> /* Xt String, Xt app init, Xt get/set values */
#include <X11/extensions/Xinerama.h> /* XineramaQueryExtension(), XineramaQueryScreens() */

#include <Xm/Xm.h> /* Motif toolkit core */
#include <Xm/MwmUtil.h> /* manipulate MWM decorations & functions */
#include <Xm/MainW.h> /* mainWin */
#include <Xm/Form.h> /* mainForm */
#include <Xm/Label.h> /* dropdown labels */
#include <Xm/CascadeB.h> /* dropdown menus */
#include <Xm/Frame.h> /*  radioFrame */
#include <Xm/RowColumn.h> /* radioBox */
#include <Xm/ToggleB.h> /* radio toggles, imgToggle */
#include <Xm/Separator.h> /* sep */
#include <Xm/PushB.h> /* capButton */

/* changes radio toggle shape from diamond to circle to match CDE */
/*
#define CDE_ROUND_TOGGLES
*/

/* fallback X11 resources */
static String fbxres[] = 
{
	/* some optional font settings */
	"*renderTable: xft",
	"*xft*fontType: FONT_IS_XFT",
	"*xft*fontName: Roboto",
	"*xft*fontSize: 11",
	"*xft*autohint: 1",
	"*xft*lcdfilter: lcddefault",
	"*xft*hintstyle: hintslight",
	"*xft*hinting: True",
	"*xft*antialias: 1",
	"*xft*rgba: rgb",
	"*xft*dpi: 96",
	
	/*
	"*delayLabel*fontStyle: Bold",
	"*cursorLabel*fontStyle: Bold",
	"*formatLabel*fontStyle: Bold",
	*/
	
	NULL,
};

void quitNow() { exit(0); }

int timeDelay = 0; /* no time delay by default */
	void timeDelayCallback0() { timeDelay = 0; } /* no delay */
	void timeDelayCallback1() { timeDelay = 5; } /* 5 second delay */
	void timeDelayCallback2() { timeDelay = 10; } /* 10 second delay */
	void timeDelayCallback3() { timeDelay = 15; }
	void timeDelayCallback4() { timeDelay = 30; }

char cursorHidden[32] = "--hidecursor"; /* cursor is hidden by default */
	void cursorHideCallback() { strcpy(cursorHidden, "--hidecursor"); }
	void cursorShownCallback() { strcpy(cursorHidden, "\0"); } /* clear by string copying a null char */

char imgFormat[32] = "png"; /* default output format is PNG */
	void imgPNGformatCallback() { strcpy(imgFormat, "png"); }
	void imgJPEGformatCallback() { strcpy(imgFormat, "jpeg"); }

char capMode[128] = "\0"; /* default capture mode is whole desktop */
	void selectModeCallback0() { strcpy(capMode, "\0"); } /* capture whole desktop */
	void selectModeCallback1() { strcpy(capMode, "--select --bordersize=2"); } /* select rectangular area */
	void selectModeCallback2() { strcpy(capMode, "--select --bordersize=2 --color=255,0,0,0.2 --highlight"); } /* highlight single window */

int showResult = 1; /* defaults to opening the image after capture */
	void imgViewCallback() { if(showResult == 1) showResult = 0; else if(showResult == 0) showResult = 1; } /* toggle image viewer */


void snapScreen()
{
	/* buffers to hold command and time strings */
	char cmdBuffer[256];
	char timeBuffer[256];
	
	/* gets the current time using time.h */
	time_t currentTime;
	time(&currentTime);
	
	/* format is "hour:minute:second_month-day-year" where time is 24 hr and year is 4 digit */
	strftime(timeBuffer, sizeof(timeBuffer), "%T_%m-%d-%Y", localtime(&currentTime));
	
	/* if imgToggle is 1 (true) then open filename with ximaging */
	if(showResult == 1)
	{
		/* string print and format the final command line into cmdBuffer */
		snprintf(cmdBuffer, sizeof(cmdBuffer), "filename=~/snap_%s.%s; maim --quiet --delay=%d %s %s $filename && ximaging $filename -pin -g +200+200 &", 
		timeBuffer, imgFormat, timeDelay, cursorHidden, capMode); /* applies timestamp and selected options */
	}
	
	/* else output image silently */
	else if(showResult == 0)
	{
		snprintf(cmdBuffer, sizeof(cmdBuffer), "filename=~/snap_%s.%s; maim --quiet --delay=%d %s %s $filename &", 
		timeBuffer, imgFormat, timeDelay, cursorHidden, capMode);
	}
	
	/* execute final command line */
	system(cmdBuffer);
}


int main(int argc, char *argv[])
{
	XtAppContext app;

/* open top level shell */
	Widget topLevel = XtVaAppInitialize(&app, "dvscreensnap", NULL, 0, &argc, argv, fbxres, NULL);
	XtVaSetValues(topLevel,
		XmNtitle, "ScreenSnap",
		XmNiconName, " ScreenSnap ",
	NULL);
	
	/* set window manager decorations */
	int decor;
	XtVaGetValues(topLevel, XmNmwmDecorations, &decor, NULL);
		decor &= ~MWM_DECOR_BORDER;
		decor &= ~MWM_DECOR_TITLE;
		decor &= ~MWM_DECOR_MENU;
		decor &= ~MWM_DECOR_MINIMIZE;
	XtVaSetValues(topLevel, XmNmwmDecorations, decor, NULL);
	
	/* set window manager functions */
	int func;
	XtVaGetValues(topLevel, XmNmwmFunctions, &func, NULL);
		func &= ~MWM_FUNC_CLOSE;
		func &= ~MWM_FUNC_MOVE;
		func &= ~MWM_FUNC_MINIMIZE;
	XtVaSetValues(topLevel, XmNmwmFunctions, func, NULL);
	

/* main window as a manager widget */
	Widget mainWin = XtVaCreateManagedWidget("mainWin", xmMainWindowWidgetClass, topLevel, 
		XmNshadowThickness, 0, 
	NULL);
	
	
/* create main form inside mainWin */
	Widget mainForm = XtVaCreateManagedWidget("mainForm", xmFormWidgetClass, mainWin,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNrightAttachment, XmATTACH_FORM,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNshadowThickness, 1,
	NULL);
	
	Pixel main_fg;
	XtVaGetValues(mainForm, XmNforeground, &main_fg, NULL);
	
	
/* create lower control form */
	Widget lowerControlForm = XtVaCreateManagedWidget("lowerControlForm", xmFormWidgetClass, mainForm,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 2,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 2,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 2,
		XmNshadowThickness, 0,
	NULL);
	
/* decorative separator */
	Widget sep = XtVaCreateManagedWidget("sep", xmSeparatorWidgetClass, lowerControlForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNshadowThickness, 2,
	NULL);
	
	Widget capButton = XtVaCreateManagedWidget("capButton", xmPushButtonWidgetClass, lowerControlForm,
		XmNshadowThickness, 2,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep,
		XmNtopOffset, 5,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 7,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 6,
		XmNmarginWidth, 8,
		XmNmarginHeight, 5,
		XtVaTypedArg, XmNlabelString, XmRString, "Capture", 8,
	NULL);
	XtAddCallback(capButton, XmNactivateCallback, snapScreen, NULL);
	
	
	Widget cancelButton = XtVaCreateManagedWidget("cancelButton", xmPushButtonWidgetClass, lowerControlForm,
		XmNshadowThickness, 2,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep,
		XmNtopOffset, 5,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, capButton,
		XmNrightOffset, 4,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 6,
		XmNmarginWidth, 8,
		XmNmarginHeight, 5,
		XtVaTypedArg, XmNlabelString, XmRString, "Cancel", 8,
	NULL);
	XtAddCallback(cancelButton, XmNactivateCallback, quitNow, NULL);
	
	
	/* toggle to show screenshot in image viewer */
	Widget imgToggle = XmCreateToggleButton(lowerControlForm, "imgToggle", NULL, 0);
	XtVaSetValues(imgToggle,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, sep,
		XmNtopOffset, 5,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 9,
		XmNrightAttachment, XmATTACH_WIDGET,
		XmNrightWidget, cancelButton,
		XmNrightOffset, 10,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 6,
		XmNalignment, XmALIGNMENT_BEGINNING,
		XmNindicatorOn, XmINDICATOR_CHECK_BOX,
		XmNtoggleMode, XmTOGGLE_BOOLEAN,
		XmNhighlightThickness, 0,
		XmNshadowThickness, 0,
		XmNset, 1,
		XmNspacing, 7,
		XtVaTypedArg, XmNlabelString, XmRString, "Open in image viewer.", 29,
	NULL);
	XtManageChild(imgToggle);
	XtAddCallback(imgToggle, XmNvalueChangedCallback, imgViewCallback, NULL);
	
	#ifdef _NO_XIMAGING
		XtDestroyWidget(imgToggle);
	#endif
	
/* create label form */
	Widget labelForm = XtVaCreateManagedWidget("labelForm", xmFormWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 7,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 12,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, lowerControlForm,
		XmNbottomOffset, 9,
		XmNshadowThickness, 0,
		XmNfractionBase, 90,
	NULL);
	
	Widget delayLabel = XtVaCreateManagedWidget("delayLabel", xmLabelWidgetClass, labelForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNalignment, XmALIGNMENT_END,
		XtVaTypedArg, XmNlabelString, XmRString, "Delay:", 7,
	NULL);
	
	Widget cursorLabel = XtVaCreateManagedWidget("cursorLabel", xmLabelWidgetClass, labelForm,
		XmNalignment, XmALIGNMENT_END,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, delayLabel,
		XmNtopOffset, 7,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XtVaTypedArg, XmNlabelString, XmRString, "Pointer:", 9,
	NULL);
	
	Widget formatLabel = XtVaCreateManagedWidget("formatLabel", xmLabelWidgetClass, labelForm,
		XmNalignment, XmALIGNMENT_END,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, cursorLabel,
		XmNtopOffset, 7,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XtVaTypedArg, XmNlabelString, XmRString, "Format:", 8,
	NULL);
	
/* create dropdown form */
	Widget dropdownForm = XtVaCreateManagedWidget("dropdownForm", xmFormWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 7,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, labelForm,
		XmNleftOffset, -8,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, lowerControlForm,
		XmNbottomOffset, 9,
		XmNshadowThickness, 0,
	NULL);
	
	
/* time delay */
	Widget delayPulldownMenu = XmCreatePulldownMenu(dropdownForm, "delayPulldownMenu", NULL, 0);
	XtVaSetValues(delayPulldownMenu,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, main_fg,
		XmNbottomShadowColor, main_fg,
		XmNadjustMargin, 0,
		XmNentryAlignment, XmALIGNMENT_CENTER,
	NULL);
	
		Widget delayOpt0 = XtVaCreateManagedWidget("None", xmPushButtonWidgetClass, delayPulldownMenu, 
			XmNshadowThickness, 1,
			XmNmarginHeight, 5,
			XmNmarginWidth, 4,
		NULL);
		XtAddCallback(delayOpt0, XmNactivateCallback, timeDelayCallback0, NULL);
	
		Widget delayOpt1 = XtVaCreateManagedWidget("5 sec", xmPushButtonWidgetClass, delayPulldownMenu, 
			XmNshadowThickness, 1,
			XmNmarginHeight, 5,
			XmNmarginWidth, 4,
		NULL);
		XtAddCallback(delayOpt1, XmNactivateCallback, timeDelayCallback1, NULL);
		
		Widget delayOpt2 = XtVaCreateManagedWidget("10 sec", xmPushButtonWidgetClass, delayPulldownMenu, 
			XmNshadowThickness, 1,
			XmNmarginHeight, 5,
			XmNmarginWidth, 4,
		NULL);
		XtAddCallback(delayOpt2, XmNactivateCallback, timeDelayCallback2, NULL);
	
		Widget delayOpt3 = XtVaCreateManagedWidget("15 sec", xmPushButtonWidgetClass, delayPulldownMenu, 
			XmNshadowThickness, 1,
			XmNmarginHeight, 5,
			XmNmarginWidth, 4,
		NULL);
		XtAddCallback(delayOpt3, XmNactivateCallback, timeDelayCallback3, NULL);
	
		Widget delayOpt4 = XtVaCreateManagedWidget("30 sec", xmPushButtonWidgetClass, delayPulldownMenu, 
			XmNshadowThickness, 1,
			XmNmarginHeight, 5,
			XmNmarginWidth, 4,
		NULL);
		XtAddCallback(delayOpt4, XmNactivateCallback, timeDelayCallback4, NULL);
		
		
	Widget delayDropdownMenu = XmCreateOptionMenu(dropdownForm, "delayDropdownMenu", NULL, 0);
	XtVaSetValues(delayDropdownMenu,	
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 0,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNsubMenuId, delayPulldownMenu,
		XmNmarginHeight, 0,
		XmNmarginWidth, 0,
		XmNtraversalOn, 0,
	NULL);
	XtManageChild(delayDropdownMenu);
	
	
/* cursor (pointer) visibility */
	Widget cursorPulldownMenu = XmCreatePulldownMenu(dropdownForm, "cursorPulldownMenu", NULL, 0);
	XtVaSetValues(cursorPulldownMenu,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, main_fg,
		XmNbottomShadowColor, main_fg,
		XmNadjustMargin, 0,
		XmNentryAlignment, XmALIGNMENT_CENTER,
	NULL);
	
		Widget cursorOpt1 = XtVaCreateManagedWidget(" Hidden ", xmPushButtonWidgetClass, cursorPulldownMenu, 
			XmNshadowThickness, 1,
			XmNmarginHeight, 5,
			XmNmarginWidth, 4,
		NULL);
		XtAddCallback(cursorOpt1, XmNactivateCallback, cursorHideCallback, NULL);
	
		Widget cursorOpt2 = XtVaCreateManagedWidget(" Visible ", xmPushButtonWidgetClass, cursorPulldownMenu, 
			XmNshadowThickness, 1,
			XmNmarginHeight, 5,
			XmNmarginWidth, 4,
		NULL);
		XtAddCallback(cursorOpt2, XmNactivateCallback, cursorShownCallback, NULL);
		
	
	Widget cursorDropdownMenu = XmCreateOptionMenu(dropdownForm, "cursorDropdownMenu", NULL, 0);
	XtVaSetValues(cursorDropdownMenu,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, delayDropdownMenu,
		XmNtopOffset, 7,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNsubMenuId, cursorPulldownMenu,
		XmNmarginHeight, 0,
		XmNmarginWidth, 0,
		XmNtraversalOn, 0,
	NULL);
	XtManageChild(cursorDropdownMenu);
	
	
/* image format */
	Widget formatPulldownMenu = XmCreatePulldownMenu(dropdownForm, "formatPulldownMenu", NULL, 0);
	XtVaSetValues(formatPulldownMenu,
		XmNshadowThickness, 1,
		XmNmarginHeight, 1,
		XmNmarginWidth, 1,
		XmNtopShadowColor, main_fg,
		XmNbottomShadowColor, main_fg,
		XmNadjustMargin, 0,
		XmNentryAlignment, XmALIGNMENT_CENTER,
	NULL);
	
	
	Widget formatOpt1 = XtVaCreateManagedWidget("PNG", xmPushButtonWidgetClass, formatPulldownMenu, 
		XmNshadowThickness, 1,
		XmNmarginHeight, 5,
		XmNmarginWidth, 8,
	NULL);
	XtAddCallback(formatOpt1, XmNactivateCallback, imgPNGformatCallback, NULL);
	
	
	Widget formatOpt2 = XtVaCreateManagedWidget("JPEG", xmPushButtonWidgetClass, formatPulldownMenu, 
		XmNshadowThickness, 1,
		XmNmarginHeight, 5,
		XmNmarginWidth, 8,
	NULL);
	XtAddCallback(formatOpt2, XmNactivateCallback, imgJPEGformatCallback, NULL);
	
	
	Widget formatDropdownMenu = XmCreateOptionMenu(dropdownForm, "formatDropdownMenu", NULL, 0);
	XtVaSetValues(formatDropdownMenu,
		XmNtopAttachment, XmATTACH_WIDGET,
		XmNtopWidget, cursorDropdownMenu,
		XmNtopOffset, 7,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNsubMenuId, formatPulldownMenu,
		XmNmarginHeight, 0,
		XmNmarginWidth, 0,
		XmNtraversalOn, 0,
	NULL);
	XtManageChild(formatDropdownMenu);
	
	
	Dimension delayDropdownMenu_h;
	XtVaGetValues(delayDropdownMenu, XmNheight, &delayDropdownMenu_h, NULL);
	XtVaSetValues(delayLabel, XmNheight, delayDropdownMenu_h, NULL);
	XtVaSetValues(cursorLabel, XmNheight, delayDropdownMenu_h, NULL);
	XtVaSetValues(formatLabel, XmNheight, delayDropdownMenu_h, NULL);
	
	/* get colors from capButton */
	Pixel pb_fg, pb_bg, pb_ts, pb_bs;
	XtVaGetValues(capButton,
		XmNforeground, &pb_fg, /* foreground */
		XmNbackground, &pb_bg, /* background */
		XmNtopShadowColor, &pb_ts, /* top shadow */
		XmNbottomShadowColor, &pb_bs, /* bottom shadow */
	NULL);
	
	/* apply colors to dropdown menus */
	Widget delayDropdownOB = XtNameToWidget(delayDropdownMenu, "*OptionButton");
	if(delayDropdownOB)
	{
		XtVaSetValues(delayDropdownOB,
			XmNforeground, pb_fg,
			XmNbackground, pb_bg,
			XmNtopShadowColor, pb_ts,
			XmNbottomShadowColor, pb_bs,
		NULL);
	}
	
	Widget cursorDropdownOB = XtNameToWidget(cursorDropdownMenu, "*OptionButton");
	if(cursorDropdownOB)
	{
		XtVaSetValues(cursorDropdownOB,
			XmNforeground, pb_fg,
			XmNbackground, pb_bg,
			XmNtopShadowColor, pb_ts,
			XmNbottomShadowColor, pb_bs,
		NULL);
	}
	
	Widget formatDropdownOB = XtNameToWidget(formatDropdownMenu, "*OptionButton");
	if(formatDropdownOB)
	{
		XtVaSetValues(formatDropdownOB,
			XmNforeground, pb_fg,
			XmNbackground, pb_bg,
			XmNtopShadowColor, pb_ts,
			XmNbottomShadowColor, pb_bs,
		NULL);
	}
	
	
/* create radio toggle form */
	Widget radioForm = XtVaCreateManagedWidget("radioForm", xmFormWidgetClass, mainForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 3,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, dropdownForm,
		XmNleftOffset, 1,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 2,
		XmNbottomAttachment, XmATTACH_WIDGET,
		XmNbottomWidget, lowerControlForm,
		XmNbottomOffset, 8,
		XmNshadowThickness, 0,
	NULL);
	
	/* create a frame to hold radio box and toggles */
	Widget radioFrame = XtVaCreateManagedWidget("radioFrame", xmFrameWidgetClass, radioForm,
		XmNtopAttachment, XmATTACH_FORM,
		XmNtopOffset, 6,
		XmNleftAttachment, XmATTACH_WIDGET,
		XmNleftWidget, cursorDropdownMenu,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 7,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNshadowThickness, 2,
		XmNshadowType, XmSHADOW_ETCHED_IN,
		XmNbottomOffset, 0,
	NULL);

/* create radio box */
	Widget radioBox = XmCreateRadioBox(radioFrame, "radioBox", NULL, 0);
	XtVaSetValues(radioBox,
		XmNtopAttachment, XmATTACH_FORM,
		XmNleftAttachment, XmATTACH_FORM,
		XmNleftOffset, 8,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 8,
		XmNorientation, XmVERTICAL,
		XmNpacking, XmPACK_TIGHT,
		XmNspacing, 9,
		XmNmarginHeight, 10,
		XmNmarginWidth, 10,
	NULL);

	/* toggle 1 */
	Widget radio1 = XmCreateToggleButton(radioBox, "Whole Desktop", NULL, 0);
	XtVaSetValues(radio1,
		XmNshadowThickness, 0,
		XmNhighlightThickness, 0,
		#ifdef _CDE_ROUND_TOGGLES
		XmNindicatorType, XmONE_OF_MANY_ROUND,
		#else
		XmNindicatorType, XmONE_OF_MANY_DIAMOND,
		#endif
		XmNset, 0,
		XmNmarginTop, 0,
		XmNmarginBottom, 0,
		XmNspacing, 7,
		XmNset, 1,
		XmNmarginRight, 0,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
	NULL);
	XtManageChild(radio1);
	XtAddCallback(radio1, XmNvalueChangedCallback, selectModeCallback0, NULL);
	
	/* toggle 2 */
	Widget radio2 = XmCreateToggleButton(radioBox, "Select Area", NULL, 0);
	XtVaSetValues(radio2,
		XmNshadowThickness, 0,
		XmNhighlightThickness, 0,
		#ifdef _CDE_ROUND_TOGGLES
		XmNindicatorType, XmONE_OF_MANY_ROUND,
		#else
		XmNindicatorType, XmONE_OF_MANY_DIAMOND,
		#endif
		XmNset, 0,
		XmNmarginTop, 0,
		XmNmarginBottom, 0,
		XmNspacing, 7,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
	NULL);
	XtManageChild(radio2);
	XtAddCallback(radio2, XmNvalueChangedCallback, selectModeCallback1, NULL);
	
	/* toggle 3 */
	Widget radio3 = XmCreateToggleButton(radioBox, "Select Window ", NULL, 0);
	XtVaSetValues(radio3,
		XmNshadowThickness, 0,
		XmNhighlightThickness, 0,
		#ifdef _CDE_ROUND_TOGGLES
		XmNindicatorType, XmONE_OF_MANY_ROUND,
		#else
		XmNindicatorType, XmONE_OF_MANY_DIAMOND,
		#endif
		XmNset, 0,
		XmNmarginTop, 0,
		XmNmarginBottom, 0,
		XmNspacing, 7,
		XmNrightAttachment, XmATTACH_FORM,
		XmNrightOffset, 0,
		XmNbottomAttachment, XmATTACH_FORM,
		XmNbottomOffset, 0,
	NULL);
	XtManageChild(radio3);
	XtAddCallback(radio3, XmNvalueChangedCallback, selectModeCallback2, NULL);
	
	/* close radioBox */
	XtManageChild(radioBox);



/* close top level */
	XtRealizeWidget(topLevel);


/* give capture button initial focus */
	XmProcessTraversal(capButton, XmTRAVERSE_CURRENT);
	

/* center the window */
	Display *display = XtDisplay(topLevel);
	Window window = XtWindow(topLevel);
	
	/* check for xinerama extension info */
	int event_base, error_base;
	
	if(!XineramaQueryExtension(display, &event_base, &error_base)) 
	{
		fprintf(stderr, "ScreenSnap: Could not query Xinerama extensions.\n");
		fflush(stderr);
		XtAppMainLoop(app);
		return 1;
	}
	
	/* get screen count */
	int screen_count;
	XineramaScreenInfo *screen_info = XineramaQueryScreens(display, &screen_count);
	
	if(!screen_info || screen_count == 0)
	{
		fprintf(stderr, "ScreenSnap: Could not retrieve Xinerama screen info.\n");
		fflush(stderr);
		XtAppMainLoop(app);
		return 1;
	}
	
	/* get window attributes like height and width */
	XWindowAttributes windowAttr;
	XGetWindowAttributes(display, window, &windowAttr);
	int windowWidth = windowAttr.width;
	int windowHeight = windowAttr.height;
	
	if(screen_count > 0)
	{
		RootWindow(display, screen_info[0].screen_number);
		
		/* take height/width of window, add screen H/W, then divide by two */
		int newX = screen_info[0].x_org + (screen_info[0].width - windowWidth) / 2;
		int newY = screen_info[0].y_org + (screen_info[0].height - windowHeight) / 2;
		
		/* finally move window to center */
		XMoveWindow(display, window, newX, newY);
	}
	
	XFree(screen_info);


/* enter main processing loop */
	XtAppMainLoop(app);
	
	return 0;
}

